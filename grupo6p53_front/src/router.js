import { createRouter, createWebHistory } from "vue-router";
import App from './App.vue';

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import TransactionCreate from './components/TransactionCreate.vue'

const routes = [{
        path: '/',
        name: 'root',
        component: App
    },
    {
        path: '/user/logIn',
        name: "logIn",
        component: LogIn
    },
    {
        path: '/user/signUp',
        name: "signUp",
        component: SignUp
    },
    {
        path: '/transaction/create',
        name: "transactionCreate",
        component: TransactionCreate
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});
export default router;